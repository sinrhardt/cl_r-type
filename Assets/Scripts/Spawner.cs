using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Pool;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    [SerializeField] private List<Enemy> enemiesPrefab;
    [SerializeField] private List<Projectile> projectilePrefab;
    [SerializeField] private int maxEnemies = 3;
    [SerializeField] private float spawnDelay = 3.5f;
    [SerializeField] private int enemiesToChangeZone = 5;

    private bool canSpawn = true;
    private int currentEnemies = 0;
    private int currentZone = 0;
    private int killedEnemies = 0;
    
    
    private IObjectPool<Enemy> enemiesPool;

    private void Awake()
    {
        enemiesPool = new ObjectPool<Enemy>(CreateEnemy, OnGetEnemy, OnReleaseEnemy, OnDestroyEnemy, true);
        Enemy.onEnemyDeath += EnemyDied;
    }

    private void OnDestroy()
    {
        Enemy.onEnemyDeath -= EnemyDied;
    }

    #region Enemy Pooling

    private Enemy CreateEnemy()
    {
        Enemy enemy = Instantiate(enemiesPrefab[currentZone], transform.position + new Vector3(0, Random.Range(-35f, 35f), 0),
            quaternion.identity);
        if (enemy.IsBoss)
        {
            enemy.transform.position = transform.position;
            maxEnemies = 0;
        }
        
        enemy.setProjectile(projectilePrefab[currentZone]);
        enemy.TypeOfEnemy = currentZone;
        
        enemy.SetPool(enemiesPool);
        return enemy;
    }

    private void OnGetEnemy(Enemy obj)
    {
        if (obj.TypeOfEnemy != currentZone)
        {
            CreateEnemy();
            return;
        }
        
        obj.AbilitateShooting();
        obj.ResetHp();
        

        
        obj.transform.position = transform.position + new Vector3(0, Random.Range(-35f, 35f), 0);
        obj.gameObject.SetActive(true);
    }

    private void OnReleaseEnemy(Enemy obj)
    {
        currentEnemies -= 1;
        obj.gameObject.SetActive(false);
        Debug.Log(currentEnemies);
    }

    private void OnDestroyEnemy(Enemy obj)
    {
        
        currentEnemies -= 1;
        Destroy(obj);
    }

    
    
    #endregion
    
    


    private void Update()
    {
        if (currentEnemies < maxEnemies && canSpawn)
        {
            StartCoroutine(SpawnEnemy());
            currentEnemies++;
        }

        ZoneChangeChecker();
    }

    private void ZoneChangeChecker()
    {
        if (killedEnemies == enemiesToChangeZone)
        {
            currentZone++;
            killedEnemies = 0;
        }
    }

    
    private IEnumerator SpawnEnemy()
    {
        canSpawn = false;
        enemiesPool.Get();
        yield return new WaitForSeconds(spawnDelay);
        canSpawn = true;
    }

    private void EnemyDied()
    {
        killedEnemies++;
    }
    
    
}
