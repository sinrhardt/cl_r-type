using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float shootingRate = 2.0f;
    [SerializeField] private int healthPoints = 5;
    [SerializeField] private float horizontalVelocity = 40.0f;
    [SerializeField] private bool isBoss = false;

    public bool IsBoss => isBoss;

    private int typeOfEnemy;

    public int TypeOfEnemy
    {
        get => typeOfEnemy;
        set => typeOfEnemy = value;
    }

    private int tempHp;
    private IObjectPool<Enemy> _enemyPool;
    private bool canShoot = true;
    private Projectile prefabProjectile;
    
    private IObjectPool<Projectile> projectilesPool;

    
    public delegate void EnemyDeath();
    public static event EnemyDeath onEnemyDeath;
    
    public delegate void BossDeath(bool endGame);
    public static event BossDeath onBossDeath;
    
    
    public void SetPool(IObjectPool<Enemy> enemyPool)
    {
        _enemyPool = enemyPool;
    }

    private void Awake()
    {
        projectilesPool = new ObjectPool<Projectile>(CreateProjectile, OnGetProjectile, OnReleaseProjectile, OnDestroyProjectile, true);
    }

    
    private void Start()
    {
        tempHp = healthPoints;
    }

    void Update()
    {
        // Movement
        transform.position += -1 * horizontalVelocity * Time.deltaTime * Vector3.right;
        if (canShoot)
        {
        StartCoroutine(ShootProjectile());
        }
    }

    private IEnumerator ShootProjectile()
    {
        canShoot = false;
        Projectile projectile = projectilesPool.Get();
        // Boss can shot randomly
        if (isBoss)
        {
            projectile.transform.position = transform.position + new Vector3(0, Random.Range(-35f, 35f), 0);
        }

        yield return new WaitForSeconds(shootingRate);
        canShoot = true;
    }
    
    private void Release()
    {
        _enemyPool.Release(this);
    }

    
    // If hit by player lowers HP, if HP==0 check if boss or not, then release
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("PlayerProjectile"))
        {
            Debug.Log("Enemy hit");
            tempHp--;

            if (tempHp == 0)
            {
                if (isBoss)
                {
                    onBossDeath?.Invoke(true);
                }
                else
                {
                    onEnemyDeath?.Invoke();
                }
                Release();
            }
        }
        else if (col.gameObject.CompareTag("DisableEnemy"))
        {
            Release();
        }
    }

    // Reset HP when reused
    public void ResetHp()
    {
        tempHp = healthPoints;
    }
    
    
    public void AbilitateShooting()
    {
        canShoot = true;
    }
    
    #region Projectile Pooling
    
    private Projectile CreateProjectile()
    {
        
        Projectile projectile = Instantiate(prefabProjectile, transform.position, Quaternion.identity);
        projectile.SetPool(projectilesPool);
        
        return projectile;
    }

    private void OnGetProjectile(Projectile obj)
    {
        obj.GetComponentInChildren<Renderer>().sharedMaterial =
            prefabProjectile.GetComponentInChildren<Renderer>().sharedMaterial;
        obj.gameObject.SetActive(true);
        obj.transform.position = transform.position;
    }

    private void OnReleaseProjectile(Projectile obj)
    {
        obj.gameObject.SetActive(false);
    }
    
    private void OnDestroyProjectile(Projectile obj)
    {
        Destroy(obj);
    }

    
    #endregion

    public void setProjectile(Projectile projectile)
    {
        prefabProjectile = projectile;
    }

    
}
