using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
    {
        GamePaused,
        GameRunning
    }
    
    

public class GameManager : MonoBehaviour
{

    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject winUI;
    [SerializeField] private GameObject loseUI;
    
    private GameState _gameState = GameState.GameRunning;


    
    private void Awake()
    {

        Enemy.onBossDeath += GameEnd;
        Player.onPlayerDeath += GameEnd;
    }

    private void OnDestroy()
    {
        Enemy.onBossDeath -= GameEnd;
        Player.onPlayerDeath -= GameEnd;

    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            
        if (_gameState == GameState.GameRunning)
            Pause();
        else if (_gameState == GameState.GamePaused)
            Unpause();
        }
    }

    public void Pause()
    {
        Time.timeScale = 0;
        pauseUI.SetActive(true);
    }

    public void Unpause()
    {
        Time.timeScale = 1;
        pauseUI.SetActive(false);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void GameEnd(bool result)
    {
        if (result)
        {
            winUI.SetActive(true);
        }
        else
        {
            loseUI.SetActive(true);
        }
    }
    
    
}

