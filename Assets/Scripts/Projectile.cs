using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Pool;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speed;
    private IObjectPool<Projectile> projectilePool;
    

    public void SetPool(IObjectPool<Projectile> pool)
    {
        projectilePool = pool;
    }
    
    // Update is called once per frame
    void Update()
    {
        transform.position +=  speed * Time.deltaTime * Vector3.right;
    }

    private void Release()
    {
        projectilePool.Release(this);
    }

    // If projectile hit the entity launching it or another projectile ignore
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (CompareTag("PlayerProjectile") && col.gameObject.CompareTag("Player"))
        {
            return;
        }
        if (CompareTag("EnemyProjectile") && col.gameObject.CompareTag("Enemy"))
        {
            return;
        }

        if (col.gameObject.CompareTag("EnemyProjectile") || col.gameObject.CompareTag("PlayerProjectile"))
        {
            return;
        }
        
        Release();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        Release();
    }
}
