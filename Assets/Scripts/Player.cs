using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.Rendering;

public class Player : MonoBehaviour
{
    [SerializeField] private float verticalVelocity = 65f;
    [SerializeField] private float horizontalVelocity = 45f;
    [SerializeField] private float shootingRate = 1.0f;
    [SerializeField] private int healthPoints = 10;

    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private Camera _camera;
    [SerializeField] private Projectile projectilePrefab;

    private IObjectPool<Projectile> projectilePool;
    private bool canShoot = true;
    
    public delegate void PlayerDeath(bool endGame);

    public static event PlayerDeath onPlayerDeath;
    
    private void Awake()
    {
        projectilePool = new UnityEngine.Pool.ObjectPool<Projectile>(CreateProjectile, OnGetProjectile, OnReleaseProjectile, OnDestroyProjectile, true, 10);
    }


    private void Start()
    {
        SetHealthPoints();
    }

    void Update()
    {
        Movement();

        if (Input.GetKey(KeyCode.Z) && canShoot)
        {
            StartCoroutine(ShootProjectile());
        }
    }

    private IEnumerator ShootProjectile()
    {
        canShoot = false;
        projectilePool.Get();
        yield return new WaitForSeconds(shootingRate);
        canShoot = true;
    }
    

    #region Movement
    void Movement()
    {
        Vector3 position = transform.localPosition;
        Vector3 playerInput = new Vector3();
        playerInput.x = Input.GetAxis("Horizontal") * horizontalVelocity;
        playerInput.y = Input.GetAxis("Vertical") * verticalVelocity;
        position += Time.deltaTime * playerInput;
        
        if (PlayerInCamera(position))
            transform.localPosition = position;
    }

    // Scale player position + next movement in camera
    // from 0 to 1 in both X and Y axes to check if hes inside the allowed area
    private bool PlayerInCamera(Vector3 position)
    {
        Vector3 viewPos = _camera.WorldToViewportPoint(position);
        if (viewPos.x <= 0.05f || 0.95f <= viewPos.x || viewPos.y <= 0.05f || 0.95f <= viewPos.y)
            return false;
        
        return true;
    }
    
    #endregion

    #region Pooling
    
    
    private Projectile CreateProjectile()
    {
        Projectile projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        projectile.SetPool(projectilePool);
        return projectile;
    }

    private void OnGetProjectile(Projectile obj)
    {
        obj.gameObject.SetActive(true);
        obj.transform.position = transform.position;
    }

    private void OnReleaseProjectile(Projectile obj)
    {
        
        obj.gameObject.SetActive(false);
    }
    
    private void OnDestroyProjectile(Projectile obj)
    {
        Destroy(obj);
    }

    #endregion

    private void SetHealthPoints()
    {
        String temp = "";
        for (int i = 0; i < healthPoints; i++)
        {
            temp += "●";
        }

        healthText.text = temp;
    }

    // If hit by enemy or enemy projectile loses health
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (!col.gameObject.CompareTag("PlayerProjectile"))
        {
        healthPoints--;
        SetHealthPoints();
        }

        if (healthPoints == 0)
        {
            onPlayerDeath?.Invoke(false);
        }
        
    }
    
    
}

